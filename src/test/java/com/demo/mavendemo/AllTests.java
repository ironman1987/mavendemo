package com.demo.mavendemo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)  
@SuiteClasses({ EvenNumberCheckerTest.class, EvenNumberCheckerTest1.class })  
public class AllTests {  
   
}
