package com.jianjiao8.common.util;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.font.TextAttribute;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.AttributedString;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


public class ImageUtils {

   
    public static String IMAGE_TYPE_GIF = "gif";// 图形交换格式
    public static String IMAGE_TYPE_JPG = "jpg";// 联合照片专家组
    public static String IMAGE_TYPE_JPEG = "jpeg";// 联合照片专家组
    public static String IMAGE_TYPE_BMP = "bmp";// 英文Bitmap（位图）的简写，它是Windows操作系统中的标准图像文件格式
    public static String IMAGE_TYPE_PNG = "png";// 可移植网络图形
    public static String IMAGE_TYPE_PSD = "psd";// Photoshop的专用格式Photoshop

   
    public static void main(String[] args) throws IOException {
        // 1-缩放图像：
        // 方法一：按比例缩放
//        ImageUtils.scale("src/img/abc.jpg", "src/img/abc_scale.jpg", 2, true);//测试OK
        // 方法二：按高度和宽度缩放
//        ImageUtils.scale2("src/img/abc.jpg", "src/img/abc_scale2.jpg", 500, 300, true);//测试OK

        // 2-切割图像：
        // 方法一：按指定起点坐标和宽高切割
//        ImageUtils.cut("src/img/abc.jpg", "src/img/abc_cut.jpg", 0, 0, 400, 400 );//测试OK
        // 方法二：指定切片的行数和列数
//        ImageUtils.cut2("src/img/abc.jpg", "src/img/abc_cut2.jpg", 2, 2 );//测试OK
//        // 方法三：指定切片的宽度和高度
//        ImageUtils.cut3("e:/abc.jpg", "e:/", 300, 300 );//测试OK
//
//        // 3-图像类型转换：
//        ImageUtils.convert("src/img/abc.jpg", "GIF", "src/img/abc.gif");//测试OK
//
//        // 4-彩色转黑白：
//        ImageUtils.gray("src/img/abc.jpg", "src/img/abc_gray.jpg");//测试OK
//
//        // 5-给图片添加文字水印：
//        // 方法一：
        ImageUtils.pressText("我是水印文字sdfsdfs123131","src/img/abc.png","src/img/abc_pressText.png","宋体",Font.BOLD,Color.white,80, -1, -1, 1f);//测试OK
//        // 方法二
        ImageUtils.pressText2("我也是水印文字法第三方士大夫士大夫收到12313sfdsfsfdsf", "src/img/abc.png","src/img/abc_pressText.png", "黑体", 36, Color.white, 80, 0, 0, 1f);//测试OK
//        
//        // 6-给图片添加图片水印：
//        ImageUtils.pressImage("src/img/尖叫哥.jpg", "src/img/abc.jpg","src/img/abc_pressImage.jpg", 0, 0, 1f);//测试OK
//        ImageUtils.pressSeal(new SealUtil().doSealToImage("广州云品汇网络科技有限公司", null),"src/img/certificate.jpg","src/img/abc_pressSeal.jpg",  1f);
         
    }
    
   
    public final static void scale(String srcImageFile, String result,
            int scale, boolean flag) throws Exception {
        try {
            BufferedImage src = getBufferedImageByType(srcImageFile);
            int width = src.getWidth(); // 得到源图宽
            int height = src.getHeight(); // 得到源图长
            if (flag) {// 放大
                width = width * scale;
                height = height * scale;
            } else {// 缩小
                width = width / scale;
                height = height / scale;
            }
            Image image = src.getScaledInstance(width, height,
                    Image.SCALE_DEFAULT);
            BufferedImage tag = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics g = tag.getGraphics();
            g.drawImage(image, 0, 0, null); // 绘制缩小后的图
            g.dispose();
            ImageIO.write(tag, "JPEG", new File(result));// 输出到文件流
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   
    public final static void scale2(String srcImageFile, String result, int height, int width, boolean bb) throws Exception {
        try {
            double ratio = 0.0; // 缩放比例
            BufferedImage bi = getBufferedImageByType(srcImageFile);
            Image itemp = bi.getScaledInstance(width, height, bi.SCALE_SMOOTH);
            // 计算比例
            if ((bi.getHeight() > height) || (bi.getWidth() > width)) {
                if (bi.getHeight() > bi.getWidth()) {
                    ratio = (new Integer(height)).doubleValue()
                            / bi.getHeight();
                } else {
                    ratio = (new Integer(width)).doubleValue() / bi.getWidth();
                }
                AffineTransformOp op = new AffineTransformOp(AffineTransform
                        .getScaleInstance(ratio, ratio), null);
                itemp = op.filter(bi, null);
            }
            if (bb) {//补白
                BufferedImage image = new BufferedImage(width, height,
                        BufferedImage.TYPE_INT_RGB);
                Graphics2D g = image.createGraphics();
                g.setColor(Color.white);
                g.fillRect(0, 0, width, height);
                if (width == itemp.getWidth(null))
                    g.drawImage(itemp, 0, (height - itemp.getHeight(null)) / 2,
                            itemp.getWidth(null), itemp.getHeight(null),
                            Color.white, null);
                else
                    g.drawImage(itemp, (width - itemp.getWidth(null)) / 2, 0,
                            itemp.getWidth(null), itemp.getHeight(null),
                            Color.white, null);
                g.dispose();
                itemp = image;
            }
            ImageIO.write((BufferedImage) itemp, "JPEG", new File(result));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
   
    public final static void cut(String srcImageFile, String result,
            int x, int y, int width, int height) {
        try {
            // 读取源图像
            BufferedImage bi = getBufferedImageByType(srcImageFile);
            int srcWidth = bi.getHeight(); // 源图宽度
            int srcHeight = bi.getWidth(); // 源图高度
            if (srcWidth > 0 && srcHeight > 0) {
                Image image = bi.getScaledInstance(srcWidth, srcHeight,
                        Image.SCALE_DEFAULT);
                // 四个参数分别为图像起点坐标和宽高
                // 即: CropImageFilter(int x,int y,int width,int height)
                ImageFilter cropFilter = new CropImageFilter(x, y, width, height);
                Image img = Toolkit.getDefaultToolkit().createImage(
                        new FilteredImageSource(image.getSource(),
                                cropFilter));
                BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                Graphics g = tag.getGraphics();
                g.drawImage(img, 0, 0, width, height, null); // 绘制切割后的图
                g.dispose();
                // 输出为文件
                ImageIO.write(tag, "JPEG", new File(result));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
   
    public final static void cut2(String srcImageFile, String descDir,
            int rows, int cols) {
        try {
            if(rows<=0||rows>20) rows = 2; // 切片行数
            if(cols<=0||cols>20) cols = 2; // 切片列数
            // 读取源图像
            BufferedImage bi = getBufferedImageByType(srcImageFile);
            int srcWidth = bi.getHeight(); // 源图宽度
            int srcHeight = bi.getWidth(); // 源图高度
            if (srcWidth > 0 && srcHeight > 0) {
                Image img;
                ImageFilter cropFilter;
                Image image = bi.getScaledInstance(srcWidth, srcHeight, Image.SCALE_DEFAULT);
                int destWidth = srcWidth; // 每张切片的宽度
                int destHeight = srcHeight; // 每张切片的高度
                // 计算切片的宽度和高度
                if (srcWidth % cols == 0) {
                    destWidth = srcWidth / cols;
                } else {
                    destWidth = (int) Math.floor(srcWidth / cols) + 1;
                }
                if (srcHeight % rows == 0) {
                    destHeight = srcHeight / rows;
                } else {
                    destHeight = (int) Math.floor(srcWidth / rows) + 1;
                }
                // 循环建立切片
                // 改进的想法:是否可用多线程加快切割速度
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < cols; j++) {
                        // 四个参数分别为图像起点坐标和宽高
                        // 即: CropImageFilter(int x,int y,int width,int height)
                        cropFilter = new CropImageFilter(j * destWidth, i * destHeight,
                                destWidth, destHeight);
                        img = Toolkit.getDefaultToolkit().createImage(
                                new FilteredImageSource(image.getSource(),
                                        cropFilter));
                        BufferedImage tag = new BufferedImage(destWidth,
                                destHeight, BufferedImage.TYPE_INT_RGB);
                        Graphics g = tag.getGraphics();
                        g.drawImage(img, 0, 0, null); // 绘制缩小后的图
                        g.dispose();
                        // 输出为文件
                        ImageIO.write(tag, "JPEG", new File(descDir
                                + "_r" + i + "_c" + j + ".jpg"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   
    public final static void cut3(String srcImageFile, String descDir,
            int destWidth, int destHeight) {
        try {
            if(destWidth<=0) destWidth = 200; // 切片宽度
            if(destHeight<=0) destHeight = 150; // 切片高度
            // 读取源图像
            BufferedImage bi = getBufferedImageByType(srcImageFile);
            int srcWidth = bi.getHeight(); // 源图宽度
            int srcHeight = bi.getWidth(); // 源图高度
            if (srcWidth > destWidth && srcHeight > destHeight) {
                Image img;
                ImageFilter cropFilter;
                Image image = bi.getScaledInstance(srcWidth, srcHeight, Image.SCALE_DEFAULT);
                int cols = 0; // 切片横向数量
                int rows = 0; // 切片纵向数量
                // 计算切片的横向和纵向数量
                if (srcWidth % destWidth == 0) {
                    cols = srcWidth / destWidth;
                } else {
                    cols = (int) Math.floor(srcWidth / destWidth) + 1;
                }
                if (srcHeight % destHeight == 0) {
                    rows = srcHeight / destHeight;
                } else {
                    rows = (int) Math.floor(srcHeight / destHeight) + 1;
                }
                // 循环建立切片
                // 改进的想法:是否可用多线程加快切割速度
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < cols; j++) {
                        // 四个参数分别为图像起点坐标和宽高
                        // 即: CropImageFilter(int x,int y,int width,int height)
                        cropFilter = new CropImageFilter(j * destWidth, i * destHeight,
                                destWidth, destHeight);
                        img = Toolkit.getDefaultToolkit().createImage(
                                new FilteredImageSource(image.getSource(),
                                        cropFilter));
                        BufferedImage tag = new BufferedImage(destWidth,
                                destHeight, BufferedImage.TYPE_INT_RGB);
                        Graphics g = tag.getGraphics();
                        g.drawImage(img, 0, 0, null); // 绘制缩小后的图
                        g.dispose();
                        // 输出为文件
                        ImageIO.write(tag, "JPEG", new File(descDir
                                + "_r" + i + "_c" + j + ".jpg"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   
    public final static void convert(String srcImageFile, String formatName, String destImageFile) {
        try {
            File f = new File(srcImageFile);
            f.canRead();
            f.canWrite();
            BufferedImage src = getBufferedImageByType(srcImageFile);
            ImageIO.write(src, formatName, new File(destImageFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   
    public final static void gray(String srcImageFile, String destImageFile) throws Exception {
        try {
            BufferedImage src = getBufferedImageByType(srcImageFile);
            ColorSpace cs = ColorSpace.getInstance(ColorSpace.CS_GRAY);
            ColorConvertOp op = new ColorConvertOp(cs, null);
            src = op.filter(src, null);
            ImageIO.write(src, "JPEG", new File(destImageFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   /**
    * 写文字，并保存到指定路径
    * @param pressText
    * @param srcImageFile
    * @param destImageFile
    * @param fontName
    * @param fontStyle
    * @param color
    * @param fontSize
    * @param x
    * @param y
    * @param alpha
    */
    public final static void pressText(String pressText,
            String srcImageFile, String destImageFile, String fontName,
            int fontStyle, Color color, int fontSize,int x,
            int y, float alpha) {
    	if(pressText == null ||"".equals(pressText))return;
        try {
            Image src = getBufferedImageByType(srcImageFile);
            int width = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, width, height, null);
            g.setColor(color);
            g.setFont(new Font(fontName, fontStyle, fontSize));
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                    alpha));
            // 在指定坐标绘制水印文字
            if(x == -1 && y==-1){//水平和垂直居中
            	g.drawString(pressText, (width - (getLength(pressText) * fontSize))
                        / 2 + x, (height - fontSize) / 2 + y);
            }if(x == -1 && y!=-1){//水平居中
            	g.drawString(pressText, (width - (getLength(pressText) * fontSize))
                        / 2 + x,  y);
            }else if(x != -1 && y == -1){//垂直居中
            	g.drawString(pressText, x, (height - fontSize) / 2 + y);
            }else{
            	g.drawString(pressText,  x, y);
            }
            g.dispose();
            ImageIO.write((BufferedImage) image, "JPEG", new File(destImageFile));// 输出到文件流
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 在原有对象上写文字
     * @param pressText
     * @param src
     * @param fontName
     * @param fontStyle
     * @param color
     * @param fontSize
     * @param x
     * @param y
     * @param alpha
     * @return
     */
    public final static BufferedImage pressText(String pressText,
    		BufferedImage src, String fontName,
            int fontStyle, Color color, int fontSize,int x,
            int y,List<HashMap<String, Integer>> subsections , float alpha) {
    	if(pressText == null ||"".equals(pressText))return src;
    	BufferedImage image = null; 
        try {
            int width = src.getWidth(null);
            int height = src.getHeight(null);
            image = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            /* 消除java.awt.Font字体的锯齿 */  
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,  
                                      RenderingHints.VALUE_ANTIALIAS_ON);  
            g.drawImage(src, 0, 0, width, height, null);
            g.setColor(color);
            Font font = new Font(fontName, fontStyle, fontSize);
            g.setFont(font);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                    alpha));
            AttributedString as = new AttributedString(pressText); 
            as.addAttribute(TextAttribute.FONT, font);  
            if(subsections != null){
            	for(Map<String,Integer> map:subsections){
            		as.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON,map.get("start"),map.get("end"));
            	}
            }
            //as.addAttribute(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON, 3, 5);
            // 在指定坐标绘制水印文字
            if(x == -1 && y==-1){//水平和垂直居中
            	g.drawString(as.getIterator(), (width - (getLength(pressText) * fontSize))
                        / 2 + x, (height - fontSize) / 2 + y);
            }if(x == -1 && y!=-1){//水平居中
            	//判断字符串是否为数字
            	boolean isNUM = isNumeric(pressText);
            	if(isNUM == true){//
            		g.drawString(as.getIterator(), (width - (getLength(pressText) * fontSize * 55 /100))
                            / 2 + x,  y);
            	}else{
            		g.drawString(as.getIterator(), (width - (getLength(pressText) * fontSize))
                            / 2 + x-5,  y);
            	}
            }else if(x != -1 && y == -1){//垂直居中
            	g.drawString(as.getIterator(), x, (height - fontSize) / 2 + y);
            }else{
            	g.drawString(as.getIterator(),  x, y);
            }
            g.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }
    

   
    public final static void pressText2(String pressText, String srcImageFile,String destImageFile,
            String fontName, int fontStyle, Color color, int fontSize, int x,
            int y, float alpha) {
        try {
            Image src = getBufferedImageByType(srcImageFile);
            int width = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, width, height, null);
            g.setColor(color);
            g.setFont(new Font(fontName, fontStyle, fontSize));
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                    alpha));
            // 在指定坐标绘制水印文字
            g.drawString(pressText, (width - (getLength(pressText) * fontSize))
                    / 2 + x, (height - fontSize) / 2 + y);
            g.dispose();
            ImageIO.write((BufferedImage) image, "JPEG", new File(destImageFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /**
    * 
    * @param pressImg
    * @param srcImageFile
    * @param destImageFile
    * @param w
    * @param h
    * @param x -1代表居中
    * @param y -1代表居中
    * @param alpha
    */
    public final static void pressImage(String pressImg, String srcImageFile,String destImageFile,int w,int h,
            int x, int y, float alpha) {
        if(pressImg == null ||"".equals(pressImg))return;
    	try {
            Image src = getBufferedImageByType(srcImageFile);
            int wideth = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(wideth, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, wideth, height, null);
            // 水印文件
            Image src_biao = getBufferedImageByType(pressImg);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                    alpha));
            if(x == -1 && y==-1){//水平和垂直居中
	            g.drawImage(src_biao, (wideth - w) / 2,
	                    (height - h) / 2, w, h, null);
            }if(x == -1 && y!=-1){//水平居中
            	g.drawImage(src_biao, (wideth - w) / 2,
	                    y, w, h, null);
            }else if(x != -1 && y == -1){//垂直居中
            	g.drawImage(src_biao, x,
	                    (height - h) / 2, w, h, null);
            }else{
            	g.drawImage(src_biao, x,
	                    y, w, h, null);
            }
            // 水印文件结束
            g.dispose();
            ImageIO.write((BufferedImage) image,  "JPEG", new File(destImageFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public final static BufferedImage pressWxImage(String pressImg,String uid, BufferedImage src,int w,int h,
            int x, int y, float alpha) {
        if(pressImg == null ||"".equals(pressImg))return src;
        BufferedImage image = null;
    	try {
            int wideth = src.getWidth(null);
            int height = src.getHeight(null);
            image = new BufferedImage(wideth, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, wideth, height, null);
            // 水印文件
            Image src_biao = getWxBufferedImageByType(pressImg,uid);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                    alpha));
            if(x == -1 && y==-1){//水平和垂直居中
	            g.drawImage(src_biao, (wideth - w) / 2,
	                    (height - h) / 2, w, h, null);
            }if(x == -1 && y!=-1){//水平居中
            	g.drawImage(src_biao, (wideth - w) / 2,
	                    y, w, h, null);
            }else if(x != -1 && y == -1){//垂直居中
            	g.drawImage(src_biao, x,
	                    (height - h) / 2, w, h, null);
            }else{
            	g.drawImage(src_biao, x,
	                    y, w, h, null);
            }
            // 水印文件结束
            g.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    	return image;
    }
    public final static BufferedImage pressImage(String pressImg, BufferedImage src,int w,int h,
            int x, int y, float alpha) {
        if(pressImg == null ||"".equals(pressImg))return src;
        BufferedImage image = null;
    	try {
            int wideth = src.getWidth(null);
            int height = src.getHeight(null);
            image = new BufferedImage(wideth, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, wideth, height, null);
            // 水印文件
            Image src_biao = getBufferedImageByType(pressImg);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                    alpha));
            if(x == -1 && y==-1){//水平和垂直居中
	            g.drawImage(src_biao, (wideth - w) / 2,
	                    (height - h) / 2, w, h, null);
            }if(x == -1 && y!=-1){//水平居中
            	g.drawImage(src_biao, (wideth - w) / 2,
	                    y, w, h, null);
            }else if(x != -1 && y == -1){//垂直居中
            	g.drawImage(src_biao, x,
	                    (height - h) / 2, w, h, null);
            }else{
            	g.drawImage(src_biao, x,
	                    y, w, h, null);
            }
            // 水印文件结束
            g.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    	return image;
    }
    
    
    public final static void pressSeal(BufferedImage src_biao, String srcImageFile,String destImageFile,
             float alpha) {
        try {
            Image src = getBufferedImageByType(srcImageFile);
            int width = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, width, height, null);//先画原来的图片
            // 水印文件
            int width_biao = src_biao.getWidth(null);
            int height_biao = src_biao.getHeight(null);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                    alpha));
            int x = (int)(width - (width_biao+0.1*width ));
            int y = (int)(height - (height_biao+0.08*height)); 
            g.drawImage(src_biao, x,y, width_biao, height_biao, null);//再画印章
            // 水印文件结束
            g.dispose();
            ImageIO.write((BufferedImage) image,  "JPEG", new File(destImageFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 在原来对象基础上设置
     * @param src_biao
     * @param src
     * @param alpha
     * @return
     */
    public final static BufferedImage pressSeal(BufferedImage src_biao, BufferedImage src,
            float alpha) {
    	if(src_biao == null)return src;
    	BufferedImage image = null;
       try {
           int width = src.getWidth(null);
           int height = src.getHeight(null);
           image = new BufferedImage(width, height,
                   BufferedImage.TYPE_INT_RGB);
           Graphics2D g = image.createGraphics();
           /* 消除java.awt.Font字体的锯齿 */  
           g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,  
                                     RenderingHints.VALUE_ANTIALIAS_ON);  
           g.drawImage(src, 0, 0, width, height, null);//先画原来的图片
           // 水印文件
           int width_biao = src_biao.getWidth(null);
           int height_biao = src_biao.getHeight(null);
           g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                   alpha));
           int x = (int)(width - (width_biao+0.1*width ));
           int y = (int)(height - (height_biao+0.1*height)); 
           g.drawImage(src_biao, x,y, width_biao, height_biao, null);//再画印章
           // 水印文件结束
           g.dispose();
       } catch (Exception e) {
           e.printStackTrace();
       }
       return image;
   }
   
    
    /**
     * 生成图片并保存到指定目录
     * @param pressImg
     * @param srcImageFile
     * @param destImageFile
     * @param w
     * @param h
     * @param x
     * @param y
     * @param alpha
     * @throws Exception 
     */
    public final static void pressCircleImage(String pressImg, String srcImageFile,String destImageFile,int w,int h,
            int x, int y, float alpha) throws Exception{
    	if(pressImg == null ||"".equals(pressImg))return;
    	Image src = getBufferedImageByType(srcImageFile);
        int wideth = src.getWidth(null);
        int height = src.getHeight(null);
        BufferedImage image = new BufferedImage(wideth, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        /* 消除java.awt.Font字体的锯齿 */  
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,  
                                  RenderingHints.VALUE_ANTIALIAS_ON);  
        g.drawImage(src, 0, 0, wideth, height, null);//先画原来的图片
        
    	BufferedImage bi1 = getBufferedImageByType(pressImg);
        
        // 根据需要是否使用 BufferedImage.TYPE_INT_ARGB
        BufferedImage image1 = new BufferedImage(bi1.getWidth(), bi1.getHeight(),
                BufferedImage.TYPE_INT_ARGB);
  
        Ellipse2D.Double shape = new Ellipse2D.Double(0, 0, bi1.getWidth(), bi1
                .getHeight());
         
        Graphics2D g2 = image1.createGraphics();
        /* 消除java.awt.Font字体的锯齿 */  
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,  
                                  RenderingHints.VALUE_ANTIALIAS_ON);  
        image1 = g2.getDeviceConfiguration().createCompatibleImage(bi1.getWidth(), bi1.getHeight(), 
        		Transparency.TRANSLUCENT);
        g2 = image1.createGraphics();
        g2.setComposite(AlphaComposite.Clear);
        g2.fill(new Rectangle(image1.getWidth(), image1.getHeight()));
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 1.0f));
        g2.setClip(shape);
        // 使用 setRenderingHint 设置抗锯齿
        g2.drawImage(bi1, 0, 0, null);
        g2.dispose();
        
        if(x == -1 && y==-1){//水平和垂直居中
            g.drawImage(image1, (wideth - w) / 2,
                    (height - h) / 2, w, h, null);
        }if(x == -1 && y!=-1){//水平居中
        	g.drawImage(image1, (wideth - w) / 2,
                    y, w, h, null);
        }else if(x != -1 && y == -1){//垂直居中
        	g.drawImage(image1, x,
                    (height - h) / 2, w, h, null);
        }else{
        	g.drawImage(image1, x,
                    y, w, h, null);
        }
        g.dispose();
        try {
            ImageIO.write(image, "PNG", new File(destImageFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 原有的图片对象上继续操作
     * @param pressImg
     * @param src
     * @param w
     * @param h
     * @param x
     * @param y
     * @param alpha
     * @param ellipse2D
     * @return
     * @throws Exception 
     */
    public final static BufferedImage pressCircleImage(String pressImg, BufferedImage src, int w,int h,
            int x, int y, float alpha,boolean ellipse2D) throws Exception{
    	if(pressImg == null ||"".equals(pressImg))return src;
        int wideth = src.getWidth(null);
        int height = src.getHeight(null);
        BufferedImage image = new BufferedImage(wideth, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        /* 消除java.awt.Font字体的锯齿 */  
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,  
                                  RenderingHints.VALUE_ANTIALIAS_ON);  
        g.drawImage(src, 0, 0, wideth, height, null);//先画原来的图片
        
    	BufferedImage bi1 = getBufferedImageByType(pressImg);
        
        // 根据需要是否使用 BufferedImage.TYPE_INT_ARGB
        BufferedImage image1 = new BufferedImage(bi1.getWidth(), bi1.getHeight(),
                BufferedImage.TYPE_INT_ARGB);
  
       
         
        Graphics2D g2 = image1.createGraphics();
        /* 消除java.awt.Font字体的锯齿 */  
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,  
                                  RenderingHints.VALUE_ANTIALIAS_ON);  
        image1 = g2.getDeviceConfiguration().createCompatibleImage(bi1.getWidth(), bi1.getHeight(), 
        		Transparency.TRANSLUCENT);
        g2 = image1.createGraphics();
        g2.setComposite(AlphaComposite.Clear);
        g2.fill(new Rectangle(image1.getWidth(), image1.getHeight()));
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 1.0f));
        if(ellipse2D == true){//图片变圆
        	Ellipse2D.Double shape = new Ellipse2D.Double(0, 0, bi1.getWidth(), bi1.getHeight());
        	g2.setClip(shape);
        }
        // 使用 setRenderingHint 设置抗锯齿
        g2.drawImage(bi1, 0, 0, null);
        g2.dispose();
        
        if(x == -1 && y==-1){//水平和垂直居中
            g.drawImage(image1, (wideth - w) / 2,
                    (height - h) / 2, w, h, null);
        }if(x == -1 && y!=-1){//水平居中
        	g.drawImage(image1, (wideth - w) / 2,
                    y, w, h, null);
        }else if(x != -1 && y == -1){//垂直居中
        	g.drawImage(image1, x,
                    (height - h) / 2, w, h, null);
        }else{
        	g.drawImage(image1, x,
                    y, w, h, null);
        }
        g.dispose();
        return image;
    }
  
    public final static int getLength(String text) {
        int length = 0;
        for (int i = 0; i < text.length(); i++) {
            if (new String(text.charAt(i) + "").getBytes().length > 1) {
                length += 2;
            } else {
                length += 1;
            }
        }
        if(isNumeric(text)==true){
        	return length;
        }else{
        	return length / 2;
        }
        
    }
    

    public static boolean isNumeric(String str){
    	   for(int i=str.length();--i>=0;){
    	      int chr=str.charAt(i);
    	      if(chr<48 || chr>57)
    	         return false;
    	   }
    	   return true;
    	}
    
    public final static  BufferedImage getBufferedImageByType(String path){
    	try {
	    	if(path.startsWith("http")){
	    		URLConnection uc = new URL(path).openConnection();
	    		BufferedImage bi = ImageIO.read(uc.getInputStream());
	    		return bi;
	    	}else{
	    		return ImageIO.read( new File(path));
	    	}
    	} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }
    
    public final static  BufferedImage getWxBufferedImageByType(String path,String uid) throws Exception{
//    	String targetFile = Config.imgPath+"/"+uid+".png";
//    	//先下载，再读入
//    	ShellUtil.wget(path, targetFile);
//    	return ImageIO.read(new FileInputStream(targetFile));
    	return null;
    }
    
    public static String GetImageStr(String imgFile)
    {//将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        InputStream in = null;
        byte[] data = null;
        //读取图片字节数组
        try 
        {
            in = new FileInputStream(imgFile);        
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
        //对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);//返回Base64编码过的字节数组字符串
    }
    public static boolean GenerateImage(String imgStr,String imgFilePath)
    {//对字节数组字符串进行Base64解码并生成图片
        if (imgStr == null) //图像数据为空
            return false;
        BASE64Decoder decoder = new BASE64Decoder();
        try 
        {
            //Base64解码
            byte[] b = decoder.decodeBuffer(imgStr);
            for(int i=0;i<b.length;++i)
            {
                if(b[i]<0)
                {//调整异常数据
                    b[i]+=256;
                }
            }
            //生成jpeg图片
            OutputStream out = new FileOutputStream(imgFilePath);    
            out.write(b);
            out.flush();
            out.close();
            return true;
        } 
        catch (Exception e) 
        {
            return false;
        }
    }
   
}